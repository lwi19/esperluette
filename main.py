#! /usr/bin/python3.9
"""
Changement de base de 10 à 2
par trois règles simples:


    Règle r1: &x→0&x
    Règle r2: x0&&y→x&0y
    Règle r3: x0&y→x1y


algorithme:
on applique r1 ou r2 dans n'importe ordre tant qu'il est possible
puis r3
"""

import time

def r_0(nums: int):
    return "&" * nums


def r_1(num_in_str):
    # Applique la règle #1
    # r1: &x→0&x
    result = "0" + num_in_str
    return result


def r_2(num_in_str):
    # Applique la règle #2
    # r2: x0&&y→x&0y ou remplacé "0&&" par "&0"
    # print("*", end="")
    result = num_in_str.replace("0&&", "&0",1)
    return result


def r_3(num_in_str):
    # Applique la règle #3
    # r3: x0&y→x1y ou remplacé "0&" par "1"
    result = num_in_str.replace("0&", "1")
    return result


if __name__ == '__main__':
    start = time.time()
    num = 1_000
    num_esp = r_0(num)
    while num_esp.startswith("&"):
        num_esp = r_1(num_esp)
        while "0&&" in num_esp:
            num_esp = r_2(num_esp)
    num_esp = r_3(num_esp)
    print(F" {num:_d} = {num_esp} en binaire,  calculé en {time.time() - start} secondes")
