# A145679
# http://oeis.org/search?q=A158625

print("A158625")
a = 5
i = 0
x = a
limit = 20
while i < limit:
    i += 1
    print(F" i, a, x = {i}: {a},{x}")
    for j in range(5):
        if (a + j * 10 ** i) % (5 ** (i + 1)) == 0:
            x = j
            a += j * 10 ** i

# A145679
# http://oeis.org/search?q=A145679

print("A145679")
a = 2
i = 0
x = a
limit = 20
while i < limit:
    i += 1
    print(F" i: x = {i}: {x}", end='\n')
    if a % 2 ** (i + 1) == 0:
        x = 0
    else:
        x = 1
        a += 10 ** i
