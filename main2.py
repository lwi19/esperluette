#! /usr/bin/python3.9
"""
Changement de base de 10 à 2  Décimal codé binaire
par six règles simples:

Règle r1: &x → 0&x
Règle r2: x0&&&&&&&&&&y → x&0y
Règle r3: x0&y → x00&y
Règle r4: x0&&y → x&0y
Règle r5: x0&y → x1y
Règle r6: x00y → x000y

algorithme:
on applique r1 ou r2 dans n'importe ordre tant qu'il est possible
puis r3, r4
puis r5, r6

exemple: 2307
r1 & r2 donne:  0&&0&&&00&&&&&&&     ok
r3 & r4 donne:  00&000&0&000&0&0&
                00&&00&&&000&&&&&&&
r5 & r6 donne:  010011000111
"""
import time


def r_0(nums: int):
    return "&" * nums


def r_1(num_in_str):
    # Applique la règle #1
    # r1: &x→0&x
    result = "0" + num_in_str
    return result


def r_2(num_in_str):
    # Applique la règle #2
    # r2: x0&&&&&&&&&&y → x&0y
    result = num_in_str.replace("0&&&&&&&&&&", "&0")
    return result


def r_3(num_in_str):
    # Applique la règle #3
    # r3: x0&y → x00&y
    result = num_in_str.replace("0&", "00&")
    return result


def r_4(num_in_str):
    # Applique la règle #4
    # r4: x0&&y → x&0y
    result = num_in_str.replace("0&&", "&0")
    return result


def r_5(num_in_str):
    # Applique la règle #4
    # r5: x0&y → x1y
    result = num_in_str.replace("0&", "1")
    return result


def r_6(num_in_str):
    # Applique la règle #4
    # r6: x00y → x000y
    result = num_in_str.replace("00", "000")
    return result


if __name__ == '__main__':
    start = time.time()
    num = 9

    num_esp = r_0(num)
    while num_esp.startswith("&"):
        num_esp = r_1(num_esp)
        print(F"règle r_1 : {num_esp}")
        while "0&&&&&&&&&&" in num_esp:
            num_esp = r_2(num_esp)
        print(F"règle r_2 : {num_esp}")

    num_esp = r_3(num_esp)
    print(F"règle r_3 : {num_esp}")

    num_esp = r_4(num_esp)
    print(F"règle r_4 : {num_esp}")

    num_esp = "00&000&0&000&0&0&"  # test pour 2307
    num_esp = r_5(num_esp)
    print(F"règle r_5 : {num_esp}")

    num_esp = r_6(num_esp)
    print(F"règle r_6 : {num_esp}")


    # print(F" {num:_d} = {num_esp} en binaire,  calculé en {time.time() - start} secondes")
