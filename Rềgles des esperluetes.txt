r1  &x → 0&x			
r2  x0&&y → x&0y	    
r3  x0&y → x1y

algorithme:
on applique r1 ou r2 dans n'importe ordre tant qu'il est possible
puis r3

mnémo:  tant qu'il y a au moins un &
			commence par & : applique r1
			contient &&    : applique r2
			sinon applique r3	

--------
exemple:
5 = &&&&&

r1: 0&&&&&
r2: &0&&&
r2: &&0&
r1: 0&&0&
r2: &00&
r1: 0&00&

r1, r2 ne peuvent plus s'appliquer
r3: 0&00& : 100&
r3: 100& : 101

5d = 101b
-----
1 = & 

r1:0&
r3:1
1d = 1b

-------
2 == && 10

r1: 0&&
r2: &0
r3: 0&0
r3: 010

2 = 10

-------

exemple pour 5

  5: &&&&&
→r1: 0&&&&&
→r2: &0&&&
→r2: &&0&
→r1: 0&&0&
→r2: &00&
→r1: 0&00&
→r3: 100&
→r3: 101

pour 11
 11: &&&&&&&&&&&
→r1: 0&&&&&&&&&&&
→r2: &0&&&&&&&&&
→r2: &&0&&&&&&&
→r2: &&&0&&&&&
→r2: &&&&0&&&
→r2: &&&&&0&
→r1: 0&&&&&0&
→r2: &0&&&0&
→r2: &&0&0&
→r1: 0&&0&0&
→r1: &00&0&
→r1: 0&00&0&
→r3: 100&0&
→r3: 1010&
→r3: 1011

	
	
	

